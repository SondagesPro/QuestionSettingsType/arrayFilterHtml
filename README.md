# arrayFilterHtml LimeSurvey Plugin

## Description

A LimeSurvey plugin to really filter multiple choice and array directly in HTML

When using array filter, current LimeSurvey version produce whole lines. Even unnecessary lines : hidden by fixed relevance, array filter from a previous group … etc …

This plugin find the uneeded lines and remove it directly from HTML, particulary interseting when you have a lot of elements and want to show only some.

For array filter attribute : filter question must simple array, multiple choice, multiple numeric and multiple text. Array of text, array of number or array dual scaleare not compatible.

## Installation

This plugin **need** [toolsDomDocument plugin](https://gitlab.com/SondagesPro/coreAndTools/toolsDomDocument) activated.

To setup this plugin like any other plugin in LimeSurvey : [you can follow this instructions](https://extensions.sondages.pro/).

## Home page and copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2019 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
