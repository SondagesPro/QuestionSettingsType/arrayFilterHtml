<?php
/**
 * arrayFilterHtml : a LimeSurvey plugin to filter array directly in HTML
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2020 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class arrayFilterHtml extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $name = 'arrayFilterHtml';
    protected static $description = 'Allow to filter array directly in HTML : the system check if filter is in same page and removge totally the uneeded lines.';

    public function init()
    {
        $this->subscribe('beforeQuestionRender', 'removeUneededLines');
        $this->subscribe('newQuestionAttributes', 'addFilterHtmlAttribute');
    }

    /**
    * The attribute, use readonly for 3.X version
    */
    public function addFilterHtmlAttribute()
    {
        if (!$this->getEvent()) {
          throw new CHttpException(403);
        }
        if (!Yii::getPathOfAlias('toolsDomDocument')) {
            return;
        }
        $addFilterHtmlAttribute = array(
            'arrayFilterHtml' => array(
                'name'      => 'arrayFilterHtml',
                'types'     => "FBAE;:JQMP", /* Near same as array filter (don't do single choice currently)*/
                'category'  => gT('Logic'),
                'sortorder' => 101,
                'inputtype' => 'switch',
                'caption'   => $this->gT('Remove unneded lines from HTML'),
                'default'   => '1',
            ),
        );

        if (method_exists($this->getEvent(), 'append')) {
            $this->getEvent()->append('questionAttributes', $addFilterHtmlAttribute);
        } else {
            $questionAttributes=(array)$this->event->get('questionAttributes');
            $questionAttributes=array_merge($questionAttributes, $addFilterHtmlAttribute);
            $this->event->set('questionAttributes', $questionAttributes);
        }
    }


    /**
     * Show an alert if toolsSmartDomDocument is not here
     */
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
          throw new CHttpException(403);
        }
        $oToolsSmartDomDocument = Plugin::model()->find("name=:name", array(":name"=>'toolsDomDocument'));
        if (!$oToolsSmartDomDocument) {
            $this->getEvent()->set('message', $this->gT("You must download toolsSmartDomDocument plugin"));
            $this->getEvent()->set('success', false);
        } elseif (!$oToolsSmartDomDocument->active) {
            $this->getEvent()->set('message', $this->gT("You must activate toolsSmartDomDocument plugin"));
            $this->getEvent()->set('success', false);
        }
    }

    public function removeUneededLines()
    {
        if (!$this->getEvent()) {
          throw new CHttpException(403);
        }
        if (!Yii::getPathOfAlias('toolsDomDocument')) {
            return;
        }
        $oEvent=$this->getEvent();
        $aAttributes=QuestionAttribute::model()->getQuestionAttributes($oEvent->get('qid'));
        if (empty($aAttributes['arrayFilterHtml'])) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($oEvent->get('surveyId'));

        $aoSubQuestionX=Question::model()->findAll(array(
            'condition'=>"parent_qid=:parent_qid and language=:language and scale_id=:scale_id",
            'params'=>array(":parent_qid"=>$oEvent->get('qid'),":language"=>App()->language,":scale_id"=>0),
            'index'=>'qid',
        ));
        $subQFiltered = array();
        if ($oSurvey->format != "A") {
            $aExistingSubQ = CHtml::listData($aoSubQuestionX, 'title', 'title');
            $sArrayFilter = trim($aAttributes['array_filter']);
            $sArrayFilterExclude = trim($aAttributes['array_filter_exclude']);
            if ($sArrayFilter) {
                $aArrayFilter = explode(";", $sArrayFilter);
                /* Find question : restrict to simple array, multiple choice, multiple numerci and multuple text */
                $criteria=new CDbCriteria();
                $criteria->compare('sid', $oEvent->get('surveyId'));
                $criteria->compare('parent_qid', 0);
                $criteria->addInCondition('title', $aArrayFilter);
                $criteria->compare('language', App()->language);
                if ($oSurvey->format == "G") {
                    $criteria->compare('gid', "<>".$oEvent->get('gid'));
                }
                $criteria->addInCondition('type', array("F","B","A","E","C","M","P","K","Q"));
                $oFilterQuestions = Question::model()->findAll($criteria);

                /* Check with $_SESSION value ? */
                foreach ($oFilterQuestions as $oFilterQuestion) {
                    $criteria=new CDbCriteria();
                    $criteria->compare('parent_qid', $oFilterQuestion->qid);
                    $criteria->compare('language', App()->language);
                    $criteria->addInCondition('title', $aExistingSubQ);
                    $oFilterSubQuestions = Question::model()->findAll($criteria);
                    foreach ($oFilterSubQuestions as $oFilterSubQuestion) {
                        $valueToCheck = "{".$oFilterQuestion->title."_".$oFilterSubQuestion->title.".NAOK}";
                        $currentValue = (string) LimeExpressionManager::ProcessStepString($valueToCheck, array(), 1, true);
                        if ($currentValue === "") {
                            $subQFiltered[$oFilterSubQuestion->title] = $oFilterSubQuestion->title;
                        }
                    }
                }
            }
            if ($sArrayFilterExclude) {
                $aArrayFilterExclude = explode(";", $sArrayFilterExclude);
                /* Find question : restrict to simple array, multiple choice, multiple numerci and multuple text */
                $criteria=new CDbCriteria();
                $criteria->compare('sid', $oEvent->get('surveyId'));
                $criteria->compare('parent_qid', 0);
                $criteria->addInCondition('title', $aArrayFilterExclude);
                $criteria->compare('language', App()->language);
                if ($oSurvey->format == "G") {
                    $criteria->compare('gid', "<>".$oEvent->get('gid'));
                }
                $criteria->addInCondition('type', array("F","B","A","E","C","M","P","K","Q"));
                $oFilterExcludeQuestions = Question::model()->findAll($criteria);

                /* Check with $_SESSION value */
                foreach ($oFilterExcludeQuestions as $oFilterExcludeQuestion) {
                    $criteria=new CDbCriteria();
                    $criteria->compare('parent_qid', $oFilterExcludeQuestion->qid);
                    $criteria->compare('language', App()->language);
                    $criteria->addInCondition('title', $aExistingSubQ);
                    $oFilterExcludeSubQuestions = Question::model()->findAll($criteria);
                    foreach ($oFilterExcludeSubQuestions as $oFilterExcludeSubQuestion) {
                        $valueToCheck = "{".$oFilterQuestion->title."_".$oFilterSubQuestion->title.".NAOK}";
                        $currentValue = (string) LimeExpressionManager::ProcessStepString($valueToCheck, array(), 1, true);
                        if ($currentValue !== "") {
                            $subQFiltered[$oFilterExcludeSubQuestion->title] = $oFilterExcludeSubQuestion->title;
                        }
                    }
                }
            }
        }

        $dom = new \toolsDomDocument\SmartDOMDocument();
        $dom->loadPartialHTML($oEvent->get('answers'));
        $baseLineId = "javatbd".$oEvent->get('surveyId')."X".$oEvent->get('gid')."X".$oEvent->get('qid');
        foreach ($aoSubQuestionX as $oSubQuestionX) {
            $remove = false;
            if (in_array($oSubQuestionX->title, $subQFiltered)) {
                $remove = true;
            } elseif (!is_null($oSubQuestionX->relevance) && $oSubQuestionX->relevance !== "" && trim($oSubQuestionX->relevance) !== "1") {
                $checkRelevance = LimeExpressionManager::ProcessStepString("{".$oSubQuestionX->relevance."}", array(), 1, true);
                $checkRelevanceJs = LimeExpressionManager::ProcessStepString("{".$oSubQuestionX->relevance."}", array(), 1, false);
                if (!$checkRelevance && !$checkRelevanceJs) {
                    $remove = true;
                }
            }
            if ($remove) {
                $div=$dom->getElementById($baseLineId.$oSubQuestionX->title);
                if ($div && $div->nodeType==XML_ELEMENT_NODE) {
                    $div->parentNode->removeChild($div);
                }
            }
        }
        $newHtml = $dom->saveHTMLExact();
        $oEvent->set('answers', $newHtml);
    }
}
